import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import {switchMap, debounceTime, tap, finalize} from 'rxjs/operators';
import {Observable} from 'rxjs'

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent {
	title = 'Форма отправки событий';
	isLoading: boolean = false;
	popupList = [];
	eventsList = ['Тип события 1','Тип события 2','Тип события 3','Тип события 4'];
	eventToShow = null;
	myForm;
	URL = "http://a0420739.xsph.ru/kubCr/";
  
	constructor(
		private formBuilder: FormBuilder,
		private http: HttpClient
	) {
		
	}

	ngOnInit() {
		this.myForm = this.formBuilder.group({
			fio: new FormControl('', Validators.required),
			podr: new FormControl('', Validators.required),
			theme: new FormControl('', Validators.required),
			list: new FormControl('', Validators.required),
			contentOrAttach: this.formBuilder.group({
				content: new FormControl(''),
				attach: new FormControl(''),	
			}, { validators: this.atLeastOneValidator }),
			attachSource: new FormControl(''),
			search: new FormControl('')
		});
		
		this.myForm
			.get('search')
			.valueChanges
			.pipe(
				debounceTime(300),
				tap( () => this.isLoading = true ),
				switchMap(value => this.popupSearch( value )
					.pipe(
						finalize(() => this.isLoading = false),
					)
				)
			)
			.subscribe(res => this.popupList = res.results );	
	}
	
	atLeastOneValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
		let controls = control.controls;
		if (controls) {
			let one = Object.keys(controls).findIndex(key => controls[key].value !== '');
			if (one === -1) {
				return {
					atLeastOneRequired: {
						text: 'Необходимо заполнить поле Содержание либо Вложение'
					}
				}
			}
		};
	}
	
	popupSearch(filter: any): Observable<EventsResponse> {
		return this.http.get<EventsResponse>(this.URL+"rest.php?search="+filter)
		.pipe(
			tap((response: EventsResponse) => {
				response.results = response.results
				.map( event => new EventElem(event.id, event.theme) )
				//.filter(event => event.theme.includes(filter))
				return response;
			})
		);
	}	
	
	onFileChange(event) {
		if (event.target.files.length > 0) {
			const file = event.target.files[0];
			this.myForm.patchValue({
				attachSource: file
			});
		}
	}
	
	sendForm(){
			const formData = new FormData();
			const date = new Date();
			
			for ( const key of Object.keys(this.myForm.value) ) {
				const value = this.myForm.value[key];
				formData.append(key, value);
			}
			if ( this.myForm.get("contentOrAttach").get("content").value ){
				formData.append("content", this.myForm.get("contentOrAttach").get("content").value)
			}else{
				formData.append("content", '')
			}
			
	   
			this.http.post(this.URL+"form.php", formData) 
				.subscribe(res => {
					console.log(res);
					this.myForm.reset({"search" : ""});
					alert('Uploaded Successfully.');
				})
			
	}
	
	showDetail(id: number){
		this.http.get(this.URL+"rest.php?searchById="+id) 
			.subscribe(res => {
				console.log(res);
				this.eventToShow = res;
			})
	}
	
	closeDetail(){
		this.eventToShow = null;
		return false;
	}
}

export class EventElem {
	constructor(public id: number, public theme: string) {}
}

export interface EventsResponse {
	total: number;
	results: EventElem[];
}
